from setuptools import setup

setup(
    name = 'VirtualLabs',
    version = '1.0',
    py_modules=['labs'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        labs=labs:cli
    ''',

)

