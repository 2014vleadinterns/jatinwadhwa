import sys
import re
import cPickle
import json

__master_list__ = []
__title__ = None
__body__ = None
__flag__ = 0

def new_note(title,body):
    __master_list__.append( (title , body) )
    return __master_list__

def delete_note_by_title(title):
    i = 0
    j = len(__master_list__)
    while i < j:
        if title:
            if title == __master_list__[i][0]:
                __master_list__.remove(__master_list__[i])
                i-=1
                j-=1
            
        if not title:
            if not __master_list__[i][0]:
                __master_list__.remove(__master_list__[i])
                i-=1
                j-=1
        i+=1   
    return __master_list__

def find_note(key):
     
    __find_list__ = []
    for i in __master_list__:
        if re.search(key,i[0],re.I):
            __find_list__.append(i)
        elif re.search(key,i[1],re.I):
            __find_list__.append(i)
            
    return __find_list__

def save_pickle_note():
    cPickle.dump(__master_list__, open('save.p', 'wb'))
    
def get_pickle_note():
    print cPickle.load(open('save.p', 'rb'))

def save_json_note():
    file = open('created_file_3.json', 'w+')
    json.dump(__master_list__, file)
    #f1 = open('file123.txt','wb')
    #f1.write(json.dumps(__master_list__))
    
    #cPickle.dump(__master_list__, open('save.p', 'wb'))
    
def get_json_note():
    file = open('created_file_3.json', 'r')
    print json.load(file)
    #f1 = open('file123.txt','rb') 
    #print json.loads(f1.read())
    
