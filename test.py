# -*- coding: UTF-8 -*-


import clipboard
from PIL import Image
import sqlite3

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        #print 'test1_empty test'
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        #print 'test2_reset test'
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = u'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        
        clipboard.copytext(msg)
        text = clipboard.gettext()
        assert(msg == text)

  

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world! how are you")
        
        #print 'hey'
        #print clipboard.gettext()
        #print text
        #print 'bye'
        #assert(text == "hello, world! how are you")
        assert(clipboard.getblob() == None )
        assert(clipboard.gettext() == "hello, world! how are you")
	
    def test_text_blob():
        clipboard.reset()
        clipboard.copytext("Hello")
        f = open("maria.jpg", "rb")
	blob_1 = f.read()                      
        text = clipboard.gettext()
        clipboard.copyblob(blob_1)           
        print clipboard.getblob()
        #assert(clipboard.getblob() == None)
        assert(clipboard.gettext() == "Hello")
    
    def test_blob_text():
        clipboard.reset()
        f = open("maria.jpg", "rb")
	blob_1 = f.read()
        clipboard.copyblob(blob_1)
        clipboard.copytext("Hello")
        text = clipboard.gettext()
        #assert(clipboard.getblob() == None)
        assert(clipboard.gettext() == "Hello")

    #def test_blob_text_both:
	


    def test_copy_blob():
        clipboard.reset()
        f = open("maria.jpg", "rb")
	blob_1 = f.read()
        clipboard.copyblob(blob_1)
	#print __size__
        clipboard.getblob()	
        assert(clipboard.gettext() == None)
        
    
    def test_text_reset():
        clipboard.reset()
        clipboard.copytext("is it working")
        text_1= clipboard.gettext()
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None) 
        
    def test_blob_reset():
        clipboard.reset()
        f = open("maria.jpg", "rb")
	blob_1 = f.read()
	clipboard.copyblob(blob_1)
        blob= clipboard.getblob()
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_blob_blob():
        clipboard.reset()
        f = open("maria.jpg", "rb")
	blob_1 = f.read()
        clipboard.copyblob(blob_1)
	f = open("maria.jpg", "rb")
	blob_2 = f.read()
        clipboard.copyblob(blob_2)
        
    def test_text_text():
        clipboard.reset()
        clipboard.copytext("Hello")
        clipboard.copytext("text after text")
	assert(clipboard.getblob() == None)
        assert(clipboard.gettext() == "text after text" )

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    #test_copy_hindi_text()
    test_copy_blob()
    test_text_reset()
    test_blob_text()
    test_text_blob()
    test_blob_reset()
    test_text_text()
    test_blob_blob()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        #clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
