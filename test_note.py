#test fucntions for note

import note

def test_note_all():
    
    def test_add_note():
        master_list =  note.new_note('title_1','body_1')
        master_list =  note.new_note('title_1','body_2')
        master_list =  note.new_note('title_2','body_3')
        assert(master_list == [('title_1','body_1'),('title_1','body_2'),('title_2','body_3')])
        
    
    def test_add_empty_note():
        master_list =  note.new_note('','body')
        assert( master_list == [('title_1','body_1'),('title_1','body_2'),('title_2','body_3'),('','body')])
        
    def test_delete_note():
        master_list = note.delete_note_by_title('title_2')
        assert( master_list == [('title_1','body_1'),('title_1','body_2'),('','body')]) 
    
    def test_delete_empty_note():
        master_list = note.delete_note_by_title('')
        assert(master_list == [('title_1','body_1'),('title_1','body_2')])

    def test_find_in_title_note():
        find_list = note.find_note('title')
        assert(find_list == [('title_1','body_1'),('title_1','body_2')])
            
    def test_find_in_body_note():
        find_list = note.find_note('bod')
        assert(find_list == [('title_1','body_1'),('title_1','body_2')])
            
    def test_find_empty_key_note():
        find_list = note.find_note('')
        assert(find_list == [('title_1','body_1'),('title_1','body_2')])
    
    def test_save_pickle_note():
        note.save_pickle_note()
 
    def test_get_pickle_note():
        note.get_pickle_note() 
    
    def test_save_json_note():
        note.save_json_note()
 
    def test_get_json_note():
        note.get_json_note()  

   



    test_add_note()
    test_add_empty_note() 
    test_delete_note()
    test_delete_empty_note()
    #test_find_note()
    #test_find_fail_note()
    test_find_in_title_note()
    test_find_in_body_note()
    test_save_pickle_note()
    test_get_pickle_note()
    test_save_json_note()
    test_get_json_note()
 
test_note_all()
