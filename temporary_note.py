import sys
import re

__master_list__ = []
__title__ = None
__body__ = None

def new_note(title,body):
    ___title__ = title
    __body__ = body
    __master_list__.append((title,body))
    return __title__,__body__

def delete_note_by_title(title):
    #here we will check for all those titles which will have name same as  title and delete all those titles.
    global __title__
    global __body__      
    i = 0
    j = len(__master_list__)
    while i < j:
        if title == __master_list__[i][0]:
            __master_list__.remove(__master_list__[i])
            i-=1
            j-=1
        i+=1   


def find_note(key):
    for i in __master_list__:
        if re.search(key,i[0],re.I):
            print 'key found in title of following note'
            __title__ = i[0]
            __body__ = i[1]
            print __title__
            print __body__
        elif re.search(key,i[1],re.I):
            print 'key found in body of following note'
            __title__ = i[0]
            __body__ = i[1]
            print __title__
            print __body__


def print_master_list():
    for i in __master_list__:
        print i

print 'printing master list'

print_master_list()

new_note('title_1','body_1')
new_note('title_1','body_2')
new_note('title_3','body_3')
new_note('title_4','body_4')
new_note('title_4','body_5')
new_note('title_2','')
new_note('hey','body of hey')
new_note('next is empty','')
new_note('','body of empty title')

print 'printing master list'
print_master_list()

delete_note_by_title('')
delete_note_by_title('title_1')
delete_note_by_title('title_2')
delete_note_by_title('title_3')
delete_note_by_title('title_4')


print 'printing master list'
print_master_list()
print '1'
find_note('')
print '2'
find_note('is')
print '3'
find_note('tite')

print 'printing master list'
print_master_list()
